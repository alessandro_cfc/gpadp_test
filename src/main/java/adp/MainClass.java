package adp;

import javafx.concurrent.Task;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import sun.rmi.runtime.Log;

import java.util.concurrent.TimeUnit;



public class MainClass {
    static WebDriver driver;
    public static String driverProperty1 = "webdriver.chrome.driver";
    public static String driverProperty2 = "D:\\Work\\Projects\\gpadp\\drivers\\chromedriver.exe";
    public static String standUrl = "http://test.gpadp.phoenixit.ru/";

    TaskClass taskClass;
    LoginClass loginClass;

    public static void main(String[] args) {
        System.setProperty(driverProperty1, driverProperty2);


        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(standUrl);

        TaskClass taskClass = new TaskClass(driver);
        LoginClass loginClass = new LoginClass(driver);

        loginClass.login();

        driver.get("http://test.gpadp.phoenixit.ru/task/update/178/content");

        String a = taskClass.getTaskStatus();

        System.out.println(a);

    }

}