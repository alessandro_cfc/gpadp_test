package adp;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.concurrent.TimeUnit;


/**
 * Created by ashabanyan on 20.05.2020.
 */
public class TaskClass {
    public WebDriver driver;
    static WebDriverWait wait;

    public TaskClass(WebDriver driver) {
        this.driver = driver;
    }

    //Метод клика на кнопку "Создать задачу"
    public void createTaskButtonClick() {
        driver.findElement(By.xpath("//span[@class=\"MuiButton-label\"]//span[text() = 'Создать задачу']")).click();
    }



    //Получение текста заголовка страницы "Создание задачи" - текст Новая задача
    public String getCreateTaskHeading() {
        return driver.findElement(By.xpath("//div[@class='main-container']//h3")).getText();
    }

    //Ввод название задачи
    public void setName(String name) {
        driver.findElement(By.xpath("//input[@id='taskName']")).sendKeys(name);
    }

    //Метод нажатия кнопки "Сохранить" при создании задачи
    public TaskClass saveTaskClick() {
        driver.findElement(By.xpath("//span[text()='Сохранить']")).click();
        return this;
    }

    //Получение статуса задачи в параметрах задачи
    public String getTaskStatus() {
        String taskStatus = driver.findElement(By.xpath("//span[text()='Моя задача']/../../following-sibling::*[2]")).getText();
        return taskStatus;
    }

    public String getTaskId() {
        String taskId = driver.findElement(By.xpath("//div[text()='Дата и время создания ']/preceding-sibling::*[1]")).getText();
        return taskId;
    }

    //Метод ввода данных в модальное окно адреса совершения -> нажатие кнопки "Готово -> ввода квартира
    public void setAddress(String region, String street, String house, String flatNumber ) {
        driver.findElement(By.xpath("//span[text()='Справочник адресов']/..")).click();

        //Нажимаем на поле регион
        driver.findElement(By.xpath("//input[@id='region']")).click();
        //Вводим регион и нажимаем ENTER
        driver.findElement(By.xpath("//input[@id='region']")).sendKeys(region);
        driver.findElement(By.xpath("//input[@id='region']")).click();
        driver.findElement(By.xpath("//input[@id='region']")).sendKeys(Keys.ENTER);

        //Нажимаем на поле улицу
        driver.findElement(By.xpath("//input[@id='street']")).click();
        //Вводим улицу и нажимаем ENTER
        driver.findElement(By.xpath("//input[@id='street']")).sendKeys(street);
        driver.findElement(By.xpath("//input[@id='street']")).click();
        driver.findElement(By.xpath("//input[@id='street']")).sendKeys(Keys.ENTER);

        //Нажимаем на поле дом
        driver.findElement(By.xpath("//input[@id='house']")).click();
        //Вводим дом и нажимаем ENTER
        driver.findElement(By.xpath("//input[@id='house']")).sendKeys(house);
        driver.findElement(By.xpath("//input[@id='street']")).click();
        driver.findElement(By.xpath("//input[@id='house']")).sendKeys(Keys.ENTER);



        //Нажимаем ГОТОВО
        driver.findElement(By.xpath("//span[text()='Готово']/..")).click();

        //Вводим квартиру
        driver.findElement(By.xpath("//input[@name='flatNumber']")).sendKeys(flatNumber);
    }

    //Ввод даты и время в поле "Дата совершения"
    public void setDateTime(String date, String time) {
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//input[@placeholder='Введите дату']")).sendKeys(date);
        driver.findElement(By.xpath("//input[@placeholder='Введите дату']")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath("//input[@placeholder='Введите время']")).sendKeys(time);
        driver.findElement(By.xpath("//input[@placeholder='Введите время']")).sendKeys(Keys.ENTER);
    }

    public void setKoap(String article, String part, String item) {
        driver.findElement(By.xpath("//input[@id='violations_0_koap_articleNumber']")).sendKeys(article);
        driver.findElement(By.xpath("//input[@id='violations_0_koap_articleNumber']")).sendKeys(Keys.ENTER);

        driver.findElement(By.xpath("//input[@id='violations_0_koap_articlePart']")).sendKeys(part);
        driver.findElement(By.xpath("//input[@id='violations_0_koap_articlePart']")).sendKeys(Keys.ENTER);

        driver.findElement(By.xpath("//input[@id='violations_0_koap_articleItem']")).sendKeys(item);
        driver.findElement(By.xpath("//input[@id='violations_0_koap_articleItem']")).sendKeys(Keys.ENTER);

    }

    public void setEventViolComment(String violComment) {
        driver.findElement(By.xpath("//textarea[@name='eventViolComment']")).sendKeys(violComment);
    }

    public Boolean saveButtonCheck() {
        WebElement saveButton = driver.findElement(By.xpath("//span[text()='Сохранить']/.."));
        return saveButton.isEnabled();
    }

    public String deleteTaskAlert() {
        String deleteTaskAlert = driver.findElement(By.xpath("//div[text()='Задача удалена']")).getText();
        return deleteTaskAlert;
    }





}
