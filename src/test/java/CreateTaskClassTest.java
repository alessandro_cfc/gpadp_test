import adp.LoginClass;
import adp.MainClass;
import adp.TaskClass;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;
import static org.junit.Assert.assertEquals;
/**
 * Created by ashabanyan on 26.05.2020.
 */

public class CreateTaskClassTest {

    private WebDriver driver;
    private TaskClass taskClass;
    private MainClass mainClass;
    private LoginClass loginclass;

    private String taskName = "TEST_SAE_00000005";
    private String region = "Москва г";
    private String street = "Краснобогатырская ул";
    private String house = "12, к 33, стр 1";
    private String flat = "1";
    private String date = "01.01.2020";
    private String time = "10:00";
    private String articleKoap = "128";
    private String partKoap = "1";
    private String itemKoap = "4";
    private String eventViolComment = "Тестовый текст события административного правонарушения";








    //Открытие стенда в Chrome
    @Before
    public void setUp() {
        System.setProperty(mainClass.driverProperty1, mainClass.driverProperty2);
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(mainClass.standUrl);
        LoginClass loginClass = new LoginClass(driver);
        loginClass.login();
        TaskClass taskClass = new TaskClass(driver);
        taskClass.createTaskButtonClick();
    }

    //Проверка соответствия титульного заголовка на странице создания задаче
    @Test
    public void createTaskHeadingTest() {
        TaskClass taskClass = new TaskClass(driver);
        String heading = taskClass.getCreateTaskHeading();
        assertEquals("Новая задача", heading);
    }

    //Создание задачи и проверка успешного сохранения путем проверки присвоения задаче статуса "Новая"
    @Test
    public void saveTaskOnlyTaskNameTest() {
        TaskClass taskClass = new TaskClass(driver);
        taskClass.setName(taskName);
        taskClass.saveTaskClick();
        WebElement dynamicSaved = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[text()='Сохранено успешно']")));
        String actualTaskStatus = taskClass.getTaskStatus();
        //Assert.assertEquals("Новая", actualTaskStatus);
        Assert.assertNotNull(taskClass.getTaskId());
    }

    //Ввод адреса
    @Test
    public void setAddress() {
        TaskClass taskClass = new TaskClass(driver);
        taskClass.setAddress(region, street, house, flat);
    }

    //Ввод даты и время совершения
    @Test
    public void setDateTime() {
        String date = "01.01.2020";
        String time = "10:00";
        TaskClass taskClass = new TaskClass(driver);

        taskClass.setDateTime(date, time);
    }

    //Ввод КоАП
    @Test
    public void setKoap() {
        TaskClass taskClass = new TaskClass(driver);
        taskClass.setDateTime(date, time);
        taskClass.setKoap(articleKoap, partKoap, itemKoap);
    }

    //Ввод текста в событие административного правонарушения
    @Test
    public void setViolCommentCheck() {
        TaskClass taskClass = new TaskClass(driver);
        taskClass.setEventViolComment(eventViolComment);
    }

    //Сохранение задачи с заполнением всех обязательных полей
    @Test
    public void saveTaskFull() {
        TaskClass taskClass = new TaskClass(driver);
        taskClass.setName(taskName);
        taskClass.setAddress(region, street, house, flat);
        taskClass.setDateTime(date, time);
        taskClass.setKoap(articleKoap,partKoap,itemKoap);
        taskClass.setEventViolComment(eventViolComment);
        taskClass.saveTaskClick();
        WebElement dynamicSaved = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[text()='Сохранено']")));
        String actualTaskStatus = taskClass.getTaskStatus();
        Assert.assertEquals("Новая", actualTaskStatus);
        Assert.assertNotNull(taskClass.getTaskId());
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}


