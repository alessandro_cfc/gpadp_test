import adp.MainClass;
import adp.TaskClass;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import sun.applet.Main;

import java.util.concurrent.TimeUnit;
import static org.junit.Assert.assertEquals;

/**
 * Created by ashabanyan on 26.05.2020.
 */

public class DeleteTaskClassTest {

    private WebDriver driver;
    private CreateTaskClassTest createTaskClassTest;
    private MainClass mainClass;
    public String taskIdA;

    //Открытие стенда в Chrome
    @Before
    public void setUp() {
        System.setProperty(mainClass.driverProperty1, mainClass.driverProperty2);
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(mainClass.standUrl);
    }

    @Test
    public void DeleteTaskTest() {
        TaskClass taskClass = new TaskClass(driver);

        taskClass.createTaskButtonClick();
        taskClass.setName("SAE_01");
        taskClass.saveTaskClick();
        WebElement dynamicId = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[text()='Новая']")));

        driver.findElement(By.xpath("//span[text()='Удалить задачу']")).click();
        driver.findElement(By.xpath("//div[@role='presentation']//span[text()='Да']")).click();
        Assert.assertEquals("Задача удалена", taskClass.deleteTaskAlert());

    }








    @After
    public void tearDown() {
        driver.quit();
    }


}
